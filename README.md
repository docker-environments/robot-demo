# Simple Docker Setup for Robot framework

## Installation

- Build the containers with
```
docker-compose build
```
- Boot up the images with:
```
docker-compose up -d db
docker-compose up -d webserver
```
- Copy the kickoff script from `config/` into the `html/` folder:
```
cp config/kickoff.sh html/
```
- Log into the webserver container:
```
docker exec -it robot-demo_webserver_1 bash
```
- In the container make the script executable and run it:
```
chmod a+x kickoff.sh
./kickoff.sh
```
- If you want to access the wordpress from your local machine, add the server to your `/etc/hosts` file:
```
127.0.0.1     webserver
```
- Run the robot test:
```
docker-compose up robot
```
The test should run through. The output will be in `reports/report.html`<br>
The test steps are defined in `tests/wordpress-login.robot`.

Shutting down is really simple:
```
docker-compose stop
```
