*** Settings ***
Documentation  Testing the login process
Library  SeleniumLibrary
Library  OperatingSystem
Library  String

*** Variables ***
${BROWSER}  Chrome
${WP LOGIN URL}  http://webserver/wp-login.php
${WP LOGIN TEXT}  Username or Email Address
${ADMIN USER}  admin
${ADMIN PASSWORD}  adminpw
${LOGIN FIELD ID}  user_login
${PASSWORD FIELD ID}  user_pass
${LOGIN SUBMIT BUTTON}  wp-submit
${DASHBOARD TEXT}  At a Glance

*** Test Cases ***
Test Login:
    Open Browser To Login Page
    Login As Admin

*** Keywords ***
Open Browser To Login Page
    Open Browser  ${WP LOGIN URL}  ${BROWSER}
    Page Should Contain    ${WP LOGIN TEXT}

Login As Admin
    Input Text    ${LOGIN FIELD ID}    ${ADMIN USER}
    Input Text    ${PASSWORD FIELD ID}    ${ADMIN PASSWORD}
    Click Button    ${LOGIN SUBMIT BUTTON}
    Page Should Contain    ${DASHBOARD TEXT}
